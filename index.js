exports.handler = async (event, context, callback) => {

    var nome = event.currentIntent.slots.Nome;
    var email = event.currentIntent.slots.Email;
    var telefone = event.currentIntent.slots.Telefone;
    var endereco = event.currentIntent.slots.Endereco;
    var empresa = event.currentIntent.slots.Empresa;

    var obj = {
        "nome": nome,
        "email": email,
        "telefone": telefone,
        "endereco": endereco,
        "empresa": empresa,
        "categoriaDaEmpresa": "",
        "anosDeMercado": 0
    }

    var http = require('http');

    var post_options = {
        host: 'hackathon-sebrae.duckdns.org',
        path: '/pessoas',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    var post_req = http.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log('Response: ' + chunk);
            context.succeed();
        });
        res.on('error', function (e) {
            console.log("Got error: " + e.message);
            context.done(null, 'FAILURE');
        });
    });

    post_req.write(JSON.stringify(obj));
    post_req.end();

    callback(null, {
        "dialogAction": {
            "type": "Close",
            "fulfillmentState": "Fulfilled",
            "message": {
                "contentType": "PlainText",
                "content": "Obrigada, " + nome + "! Já criei seu cadastro."
            }
        }
    });
};